---
layout: markdown_page
title: "Product Direction - Monorepos"
---

- TOC
{:toc}

We want to improve the monorepo experience - whether it has multiple services that all get deployed together, or if it's a mobile app in a single repo that needs to be built completely differently for iOS and Android, there are challenges that make using the monorepo unpleasant. This year, we are focusing on the build process, and working to make that efficient and understandable.

## What's next & why

- [Make CI lovable for monorepos](https://gitlab.com/groups/gitlab-org/-/epics/812#whats-next-why)

## North Stars
TBD

## Stages with monorepo focus
- [Verify](/direction/verify)

## Highlighted epics and issues

There are a few epics and important issues you can check out to see where we're headed.

- [&812](https://gitlab.com/groups/gitlab-org/-/epics/812): Make CI lovable for monorepos
